const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = process.env.PORT || 5000;

app.use(cors()); 
app.use(bodyParser.json());

const apiRouter = require("./routes/api.router");
app.use("/api/v1/todoList", apiRouter);

app.listen(port, () => {
    console.log(`Server Listening Port ${port}`);
});