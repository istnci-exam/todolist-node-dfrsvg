const router = require('express').Router();
const TodolistService = require('../services/Todolist.service');

router.get("/", async(req,res) =>{  //await와 함께 async
    try {
        const user_id = req.headers.user_id;

        const result = await TodolistService.getTodolist(user_id); //함수 자체를 비동기로 선언했기때문에 awatit처리
        //result 는 gettodolist의 리턴을 담을 변수
        res.status(200).send(result);


    } catch (error) {
        res.status(500).send(error.message);
    }
});

router.post("/", async(req,res) =>{
    try {
        const todolist = req.body;
        const result = await TodolistService.updateTodolist(todolist);

        res.status(200).send(result);
    } catch (error) {
        res.status(500).send(error.message); //라우터 객체를 갖고 있기에
    }
});


router.post("/conversation", async (req,res) => {
    try {
        const user_id = req.body.user_id;
        const result = await TodolistService.openConversation(user_id);
//1085882
        // debugger;
    } catch (error) {
        res.status(500).set(error.message);
    }
});

router.post("/message", async( req, res) => {
    try {
        const user_id = req.body.user_id;
        const resultConversation = await TodolistService.openConversation(user_id);

        const conversation_id = resultConversation.data.conversation.id;
        const resultMessage = await TodolistService.sendMessage(conversation_id);

        res.status(200).send(resultMessage.data);
// debugger;
    } catch (error) {
        res.status(500).set(error.message);
    }
})
module.exports = router;
