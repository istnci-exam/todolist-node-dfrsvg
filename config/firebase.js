const admin = require('firebase-admin');
const serviceAccount = require('./myproject-b8358-firebase-adminsdk-7eoug-dbdeda563b.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });
  
const db = admin.firestore();

module.exports = db;
